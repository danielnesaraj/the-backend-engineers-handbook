# Spring Beans

## **Singleton Pattern**

Either go for enum singleton or spring bean. **DO NOT** use double checked locking pattern.

## **Spring Beans and Dependency Injection** 

### **Configuring Spring**

A spring application configuration can either be done using XML configuration files, Java code, or both.

#### **Java code**

Prefer this approach as this can be more readable (and less verbose than XML).

* Configuration classes annotated with `@Configuration` annotation.
* Other configuration classes can be combined into this using `@Import` annotation. If the configuration is from XML, it can be imported using `@ImportResource` .
* Properties can be linked using `@PropertiesSource`  annotation
* Beans can be defined by methods in the class using `@Bean` annotation
* `@ComponentScan`  annotation can be used to configure the scan to pick up other beans that defined using `@Component` and related annotations (@Service, @Controller, @Repository).

```{code-block} java
:caption: Example
:lineno-start: 1

@Configuration
@ComponentScan(basePackages = "com.example.service.awesome")
@ImportResource("classpath:applicationcontext.xml")
@Import({AnotherAwesomeServiceConfig.class})
@PropertySource("file:/usr/local/config/application.properties")
public class AwesomeServiceConfig {    
    @Bean    
    public BeanA beanA() {        
        return new BeanA();    
    }
}
```

#### **XML configuration file**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans
	xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context"  xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd  http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-3.0.xsd">
	<context:component-scan base-package="com.example.service.awesome"/>
	<bean id="propertyPlaceholder"    class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
		<property name="systemPropertiesModeName" value="SYSTEM_PROPERTIES_MODE_OVERRIDE"/>
		<property name="locations">
			<list>
				<value>file:/usr/local/config/application.properties</value>
			</list>
		</property>
	</bean>
	<bean id="BeanA" class="com.example.service.BeanA"/>
</beans>
```

#### Name of the bean

When the annotations are used, the name of the bean by default is the simple name of the class except the first letter is changed to lowercase. (You will also have to mention the package to be scanned for beans in `component-scan` tag in `applicationcontext.xml`  or using `@ComponentScan` )

```{code-block} java
:caption: Example
:lineno-start: 1

//The name of this bean is 'testBean'
@Component
public class TestBean {}
```

This can be changed by manually specifying the bean name with the annotation

```{code-block} java
:caption: Example
:lineno-start: 1

//The name of this bean is 'testBeanExample'
//Only do this if you face conflicts with bean names; stick to defaults as much as you can
@Component("testBeanExample")
public class TestBean {}
```

When the `@Bean` annotation configuration is used, the name of the method is used as the bean name. This annotation also provides a way to override this mechanism; however, stick to naming the method properly rather than leveraging the annotation level overrides.

```{code-block} java
:caption: Example
:lineno-start: 1

@Bean
public BeanA beanA() {
	return new BeanA();
}
```


**DO NOT PREFER THIS APPROACH**

```{code-block} java
:caption: Example
:lineno-start: 1

@Bean("beanA")
public BeanA defineBeanA() {    
	return new BeanA();
}
```

#### Bean Scope

Spring Beans have many possible scopes but, most of them are related to web-aware Spring Beans. Since our codebase is not a Spring Web Application and we are simply using Spring Beans for dependency injection, we only need to concern ourselves with **`Singleton`** and **`Prototype`** scopes.

The `Singleton` scope is the default. This means that when the Spring Application Context is being created, an instance of the declared bean is created. Where ever we inject the bean (using `@Autowired` or `SpringContext`) we inject the same object everywhere. So, a **singleton Spring Bean should never store state**! Otherwise, this would cause sharing of states between threads and will not be thread-safe.

:::{danger}
**DO NOT STORE STATE IN A SINGLETON BEAN**

```java
@Component
public class SomeAwesomeBean {
	private Account account;

	public void setAccount(Account account) {
		this.account = account;    
	}
}
```
:::


For a `Prototype` bean, the scope needs to be mentioned explicitly using the `@Scope("prototype")` annotation. (Note: this annotation is not inherited. It needs to be mentioned where ever you are declaring `@Component` or other annotations even if it the class is subclasses from a bean class). A prototype bean creates a new instance of an object every time it is fetched from the context; therefore it is safe to store states in a prototype bean. But **you cannot autowire prototype bean directly into a singleton bean**. You always need to inject it using `SpringContext` i.e. using the ApplicationContext or other methods that are detailed [here](https://www.baeldung.com/spring-inject-prototype-bean-into-singleton).

#### Dependency Injection

When injecting beans, pick an approach in the following order of preference:

1.  Constructor injection

```java
//constructor injection
private final BeanE beanE;

@Autowired
public Constructor(BeanE beanE) {
	this.beanE = beanE;
}
```
2.  Field injection

```java
//field injection
@Autowired
private BeanF beanF;
```

3.  Passing dependency as parameters

```java
//dependency on BeanH and BeanK
int getCount(BeanH beanH, BeanK beanK,) {
    return beanH.getCount() + beanK.getCount();
}
```

4.  Using SpringContext in the method – this should only be used when the target class is not a Spring Bean.

```java
//using SpringContext in method
//only use when above 3 methods cannot work
int getCount() {
    BeanH beanH = SpringContext.getBean(BeanH.class);
    BeanK beanK = SpringContext.getBean(BeanH.class);
    return beanH.getCount() + beanK.getCount();
}
```

When mixing Java inheritance and Spring Bean declaration, you can confuse Spring. Take the following code for instance:

```{code-block} java
:caption: Inheritance and beans
:lineno-start: 1

@Component
public class ParentBean {}

@Component
public class ChildBean extends ParentBean { //This also applies to implements interface cases 
}

@Component
public class AnotherBean {    
	@Autowired //Fails here    
	private ParentBean parentBean;
}
```

Spring will fail to start in this case because it does not know which bean to autowire. In the background, a bean of name parentBean is created with type ParentBean and a bean of name childBean is also created with type ParentBean. To avoid failures, utilize the Qualifier annotation, like so:

```{code-block} java
:caption: @Qualifier exmaple
:lineno-start: 1

@Component
public class AnotherBean {    
	@Autowired    
	@Qualifier("parentBean")    
	private ParentBean parentBean;
}
```

But if you know the default bean that should be autowired unless explicitly specified, you may use the Primary annotation

```{code-block} java
:caption: @Primary example
:lineno-start: 1

@Component
@Primary
public class ParentBean {}

@Component
public class ChildBean extends ParentBean { //This also applies to implements interface cases
}

@Component
public class AnotherBean {    
	@Autowired //autowires parentBean since that is the primary    
	private ParentBean parentBean;
}
```

When using SpringContext instead of Autowiring, use the following:

```{code-block} java
:caption: SpringContext example
:lineno-start: 1

public SomeClass {    
	public ParentBean getChildBean() {
        return` `SpringContext.getBean("childBean", ParentBean.class);    
	}
}
```

#### Bean Lifecycle

## **Cookbook Scenarios**

### **Duplicate beans**

#### **Problem Statement**

The same class's object can be instantiated as different spring beans if they have different names

#### **Solution**

**DO NOT MANUALLY NAME BEANS**, unless you are forced to do this due to bean name conflicts

If you really must do this, then stick to Spring Bean name conventions.

:::{caution}
**DO NOT DEVIATE FROM NAMING CONVENTIONS**

```java
@Component("AnotherAwesomeBean") //Bean name starting in upper case
public class AnotherAwesomeBean {}
```
:::

Prefer the annotations way of declaring the beans. If for some reason you need to declare the bean in XML, ensure that you are sticking to the same naming conventions as explained for the annotation configuration above.

```xml
<bean id="springContext" class="com.demach.konotor.bean.SpringContext" />
```

:::{caution}
**DO NOT DEVIATE FROM NAMING CONVENTIONS**

```xml
<!-- Bean name starting in upper case -->
<bean id="SpringContext" class="com.demach.konotor.bean.SpringContext" />
```
:::

:::{danger}
**DO USE BOTH METHODS TO DEFINE BEANS.** In the best case, the application will error out stating that there are duplicate beans. In the worst case, we might have two objects of the class with different bean names at runtime.
:::

NOTE TO SELF: verify the above warning

### **Prototype beans**

#### **Problem Statement**

using prototype beans

#### **Solution**

prototype beans make sense in a full blown spring application where the use of `new`  operator is discouraged and every class should be a bean. However, we are using spring only as an inversion of control and dependency injection mechanism. We have some classes that are Spring Beans and others that are not. In case a class should not be a singleton and needs to store state, define it as a plain Java class and use with `new`  operator.\

:::{danger}
**AVOID PROTOTYPE BEANS**
```java
@Component
@Scope("prototype")
public class SomeAwesomeBean {}
```
:::

### **Many constructor parameters**

#### **Problem Statement**

There are too many parameters in constructor.

#### **Solution**

This usually means that the class is in violation of single responsibility principle. Try to refactor the class.\
In some cases, the dependency probably applies only to the method rather than the entire class. In this case, move the dependency as method parameter.

### **Initialization logic in Spring Beans**

#### **Problem Statement**

Sometimes, we need to have some initialisation or other logic which needs to be done in a Spring Bean. Such logic are usually not meant to be in the constructor. To overcome this, instance blocks are used in regular Java classes (this may not be possible sometimes as the instance blocks are executed _before_ the constructor).

#### **Solution**

In Spring Beans, you can use the `@javax.annotation.PostConstruct` annotated method within which such logic can be added. This method will be automatically executed after the constructor completes execution and before the object is available for use.

Annotating the method with `@Autowired`  has the same effect.

### **DI inside methods**

#### **Problem Statement**

Class dependency may be instantiated inside a method. This makes writing unit test for this method quite difficult.

#### **Solution**

When the dependency is instantiated in a method, try to refactor this to constructor or field. If the scope of the dependency only makes sense for the method, the dependency must be a method parameter.
