# Table of contents

* [✅ Introduction](README.md)

## Build And Other Tools

* [✅ Maven](build-and-other-tools/maven.md)
* [Styling Tools](build-and-other-tools/styling-tools.md)

## Core Java

* [Introduction](core-java/introduction.md)
* [The static modifier](core-java/the-static-modifier.md)
* [Inheritance](core-java/inheritance.md)
* [Asynchronous Processing](core-java/asynchronous-processing.md)
* [Concurrency & Parallelism](core-java/concurrency-and-parallelism.md)
* [Exception Handling](core-java/exception-handling.md)
* [Resource management](core-java/resource-management.md)
* [Boilerplate Code & Libraries](core-java/boilerplate-code-and-libraries.md)
* [Design Patterns](core-java/design-patterns.md)
* [Generics](core-java/generics.md)

## Spring

* [Spring Beans](spring/spring-beans.md)

## Common Components

* [HTTP Client](common-components/http-client.md)
* [Caching](common-components/caching.md)

## Unit Testing

* [Basics](unit-testing/basics.md)
* [Unit Tests](unit-testing/unit-tests.md)

## Databases

* [SQL](databases/sql.md)

## Observability

* [Metrics](observability/metrics.md)
* [Logging](observability/logging.md)
