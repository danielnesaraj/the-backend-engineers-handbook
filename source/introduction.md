---
description: >-
  As a backend engineer, one must work on various systems in addition to the
  application code. Across the board, certain industry and org-level best
  practices must be followed.
cover: >-
  https://images.unsplash.com/photo-1534190239940-9ba8944ea261?crop=entropy&cs=tinysrgb&fm=jpg&ixid=MnwxOTcwMjR8MHwxfHNlYXJjaHwyfHx0b29sc3xlbnwwfHx8fDE2NzA2NjkyMjU&ixlib=rb-4.0.3&q=80
coverY: 0
---

# Introduction

These best practices might be intended for several benefits. They may make the code

* more reliable: for instance, it could help make the code more testable and therefore have better unit tests which improves the reliability of the code.
* more maintainable
* or perform better

This handbook intends to capture all the recommended practices that a backend engineer could follow to improve the overall quality of the code and systems that they work on starting from the build tool, to code, to third party dependencies.
