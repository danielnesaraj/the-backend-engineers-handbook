# Unit Tests

## **Getting Started**

Note to self: write about where to write tests

## **Mocking vs Spying**

When you are writing unit tests, you are trying to ensure that a unit of code – a method in a class or few methods in a class – are working as expected. So every class should have a corresponding unit test class – this is a one-to-one relation (except when you are dealing with servlets, or DBDAOs; unit tests are important for service classes where business logic is concentrated). Class dependencies are irrelevant to the test and such classes are _**always**_ mocked. (Refer to [Class dependencies]() section above for details)

Mocking stubs all the logic in the dependency so that you can tell the unit test code to assume certain responses for a method. You can refine the mocking logic by specifying varying responses based on parameters. Usually, for multiple test cases in a unit test class, the mocking logic can be shared; in which case, leverage the `@Before` annotated method to handle this.

Sometimes, you may need to mock the logic of a method in the class that is targeted by the unit test. To do this, you can use spying. Spying, unlike mocking, works on the instance of the target class.

## **Is Powermock Evil?**

Not if you use it only in the capacity it needs to be used in. It is a double-edged sword – a powerful tool that can work against you if you do not use it properly.

Powermock is to be used only if you are working with legacy code (i.e. code that cannot easily be refactored probably because of convoluted code or a high impact area or both) or when trying to stub static methods. When powermock if overused, it can make your test case quite fragile – unrelated code changes can cause the test cases to break (may be due to issues with loading classes from SDK/libraries) and also slow down your test cases.

## **Handling Spring Beans**

A typical JUnit test has a default runner in which tests run. This will not be able to handle spring dependency injection in the code. For such cases, use the SpringJUnit4Runner. This will also allow you to specify a subset of beans that are required only for the unit test to execute.

### **Handling test for async logic** 

#### **Problem** Need to add sleep in testing code because the application code is async.
#### **Solutions** 
1. Make references of the `ExecutorService` pluggable by making them Spring Beans. This will help in mocking in unit tests.
2. Replace the `ExecutorService` reference by means of Java Reflection.
