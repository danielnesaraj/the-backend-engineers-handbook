.. the-backend-engineers-handbook documentation master file, created by
   sphinx-quickstart on Thu Jul 27 10:28:03 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

The Backend Engineer's Handbook
===============================

.. toctree::
   :maxdepth: 4
   :caption: Contents:
   :hidden:
   introduction.md

.. toctree::
   :maxdepth: 4
   :caption: Build and Other Tools:
   :hidden:

   build-and-other-tools/maven.md
   build-and-other-tools/styling-tools.md

.. toctree::
   :maxdepth: 4
   :caption: Core Java:
   :hidden:

   core-java/introduction.md
   core-java/the-static-modifier.md
   core-java/inheritance.md
   core-java/asynchronous-processing.md
   core-java/concurrency-and-parallelism.md
   core-java/exception-handling.md
   core-java/resource-management.md
   core-java/boilerplate-code-and-libraries.md
   core-java/design-patterns.md
   core-java/generics.md

.. toctree::
   :maxdepth: 4
   :caption: Spring:
   :hidden:

   spring/spring-beans.md

.. toctree::
   :maxdepth: 4
   :caption: Common Componenets:
   :hidden:

   common-components/http-client.md
   common-components/caching.md

.. toctree::
   :maxdepth: 4
   :caption: Unit Testing:
   :hidden:

   unit-testing/basics.md
   unit-testing/unit-tests.md

.. toctree::
   :maxdepth: 4
   :caption: Databases:
   :hidden:

   databases/sql.md

.. toctree::
   :maxdepth: 4
   :caption: Observability:
   :hidden:

   observability/logging.md
   observability/metrics.md

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
