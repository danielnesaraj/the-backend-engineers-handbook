---
description: >-
  Maven is a build and dependency management tool for Java. While it makes like
  easy by automating the dull aspects of building the code, it should also be
  used with care.
---
```{image} apache-maven-logo.svg
:alt: logo
:align: center
```

# Maven

A typical Java application requires developers to compile Java classes, package the resulting class files into JAR, WAR, or other package types, and package resources, etc. As a build tool, maven helps to automate these processes by means of an XML based configuration – called a POM file. This makes developers life easier and helps avoid errors.

Similarly, a Java application, could rely on third party libraries. Dependency management feature of maven allows us to simply specify the specific libraries we need to use; and maven takes care of downloading and adding the relevant dependencies to the build to package the application into a JAR/WAR – a process called dependency resolution.

## POM

The POM – Project Object Model – is a unit of work in maven. This file will contain the configuration of the project or module such as:

1. dependencies - JAR dependencies are identified by using a combination of `group` , `artifactId` , and `version` tags. So a given library that you need to use can have various versions as a publisher releases it and you are free to use the specific version you need.
2. build configuration - you may want certain tasks to be done during builds. For example, generate swagger documentation, or generate code coverage report, etc.
3. plugins - plugins help achieve certain tasks like generating fat JARs, running unit tests, etc

POM files have an inheritance structure. All POM files are inherited from a super POM. In a multi-module project, there will be a parent POM that is further inherited by POM files of modules.

```
Parent
  | |
  | +-pom.xml
  |
  +-- Module A
  |    |
  |    +-pom.xml
  |
  +-- Module B
       |
       +-pom.xml
```

### Build lifecycle

The build lifecycle is central to the build mechanism in maven. The lifecycle has multiple stages and some of the important ones are compile, test, package, install. All actions are hooked on to these lifecycle phases. The phases also occur one after another; so if a user asks for an action that comes later in the lifecycle, the previous phases are executed first.

### Resources

For further information, read these official guides:

1. [https://maven.apache.org/guides/introduction/introduction-to-the-pom.html](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html)
2. [https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html](https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html)
3. [https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)

## Cookbook Scenarios

Dependency management can get tricky and introduce unintentional issues if not used properly; particularly in a multi-module project. This section elaborates some of the common problems and how to sidestep them.

### Adding multiple related dependencies

#### **Problem statement**

In few cases like the Spring Framework, the framework is split into multiple pluggable JAR files that are added as dependencies into a project. Such libraries implicitly require only compatible versions (typically a single version number) across the different JAR dependencies that are added. Maven, however, does not have a mechanism to ensure that a developer is only using compatible versions of the libraries. This could potentially lead to bugs or unexpected behaviour.

It is even more important to ensure this compatibility in a multi-module project as it is very easy to accidentally override version numbers.

Using different versions will also make it hard to reliably upgrade libraries.

#### **Solution 1: The BOM (Bill of Materials) POM**

A BOM POM file, which is provided by the publisher of the library ensures that only compatible versions of the JAR file are put together in your project. For a given version of the BOM file, all the related libraries with compatible versions are defined by the publisher so that developers do not have to bother with this.

To leverage this, import the BOM POM file in the `dependencyManagement` section and simply declare the `group` and `artifactId` alone in the `dependencies` section. For a multi-module project, import the BOM POM file in the parent POM file so that all modules use the same versions

```xml
<dependencyManagment>
    <dependencies>
        <dependency>
            <group>com.example</group>
            <artifactId>artifacts-BOM</artifactId>
            <version>1.2.3</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagment>
<!-- ... -->
<!-- ... -->
<dependencies>
    <dependency>
        <group>com.example</group>
        <artifactId>artifact-one</artifactId>
    </dependency>
    <dependency>
        <group>com.example</group>
        <artifactId>artifact-two</artifactId>
    </dependency>
</dependencies>
```

#### **Solution 2: Use properties for version number**

If BOM POM is not available, and the compatible versions of the JAR file have the same version numbering, you can leverage the properties feature in Maven. Simply create a property for the version number and use that everywhere in POM file. Again, make sure the libraries are declared in the `dependencyManagement` section in the parent POM to ensure all child modules use the same versions.

```xml
<properties>
	<artifact.version>1.2.3</artifact.version>
</properties>
<dependencyManagment>
	<dependencies>
		<dependency>
			<group>com.example</group>
			<artifactId>artifact-one</artifactId>
			<version>${artifact.version}</version>
		</dependency>
		<dependency>
			<group>com.example</group>
			<artifactId>artifact-two</artifactId>
			<version>${artifact.version}</version>
		</dependency>
	</dependencies>
</dependencyManagment>
<!--...-->
<!--...-->
<dependencies>
	<dependency>
		<group>com.example</group>
		<artifactId>artifact-one</artifactId>
	</dependency>
	<dependency>
		<group>com.example</group>
		<artifactId>artifact-two</artifactId>
	</dependency>
</dependencies>
```

**Note:** if the related dependencies do not share version number, skip the `properties` tag alone. Continue leveraging the `dependenciesManagement` tag to ensure only compatible versions of the dependencies are used.

### Locking transitive dependencies to specific versions

#### **Problem Statement**

Maven simplifies lives by resolving transitive dependencies. But if the transitive dependencies conflict with the version of the library you have declared, the application could use either version at runtime depending on which version JAR it encounters first in classpath – a problem appropriately named [dependency hell](https://en.wikipedia.org/wiki/Dependency\_hell).

To avoid this, you'd want to ensure that the transitive dependencies introduced by other dependencies are a specific version.

#### **Solution: `dependencyManagement`**

Leverage the `dependencyManagement` feature of maven. This feature ensures that the specified version of the transitive dependency alone is used.

Common solutions online would suggest using `exclude` tag. But this solution would not scale and makes the POM file less readable and difficult to manage.

### Uniform dependencies in multi-module project

#### **Problem Statement**

Even if you are using a single dependency JAR but it is used across modules in a multi-module project, ensuring that the same version is being used will help in maintainability of application and also ensure there are no unexpected behaviour in runtime.

#### **Solution: again, `dependencyManagement`**

Use the `dependenciesManagement` tag in the parent module’s POM file and specify only the `group` and `artifactId` of `dependencies` section in the child modules wherever the dependencies are required.

### Handling niche dependencies

#### **Problem Statement**

Some dependencies may not be reliably fetched from the central repositories (OR) you forked a library and made modifications. It might be tempting to checkin the JAR file in the codebase and simply specify the path as a dependency location. But this will present problems when the library needs to be upgraded or maintain the dependency.

#### **Solution 1: fetch dependency from an alternate repository**

Try to fetch the dependency from an alternate repository as the library may be published there. To achieve this use the `repository`  tag in the parent POM. (Prefer the parent POM so that the configuration remains in a central location)

```xml
<project>
	<!--...-->
	<repositories>
		<repository>
			<id>my-internal-site</id>
			<url>https://myserver/repo</url>
		</repository>
	</repositories>
	<!--...-->
</project>
```

**Solution 2: have an org-wide maven nexus server**

Leverage organisation wide hosted maven repository if this is available to publish the library (which may be because the library is sparsely available or you have forked and modified a library's code). This will also require you to configure a repository as in Solution 1.

### Versioning modules in the project

#### **Problem Statement**

Inconsistent version numbers for modules of your application can be confusing.

#### **Solution: leverage POM inheritance**

In a multi-module project, declare the `group` , `artifactId` , and `version` in the parent POM. Declare the Version as a property. In the child modules, do not duplicate the `group` and `version`  – this is inherited from parent POM. Simply specify the `artifactId`.

### Global configuration for tools

#### **Problem statement**

Configuring some tools for a specific module might involve refactoring if the same needs to be done for other modules later.

#### **Solution**

Tools like JaCoCo which apply to all modules need to be configured in the parent POM. This will automatically be inherited by the child module POM.

### Build times should be as short as possible

#### **Problem Statement**

Builds are taking far too long

#### **Solution 1**

If this is local environment or CI, perform only necessary operations. For example, in sandbox or testing pipeline, swagger can be skipped or build only modules you require.

#### **Solution 2**

Maven profiles can be used to specify what needs to be done and activated where needed.

### Build warnings during build

#### **Problem Statement**

Warnings about file encoding and Java versions

#### **Solution**

Specify the file encoding as UTF-8 and Java source and target versions as Java 8 (or whatever is the current version at the time) in the parent POM. Ensure these are not being overridden in the child module POM.

### Building WAR and JAR files for a module

#### **Problem Statement**

Sometimes, a project may require a module to be packaged into a WAR archive and also a JAR archive. This module may be a web app on its own but its classes may also be dependencies for other modules.

#### **Solution**

It's best to avoid creating such cases. But if such a case already exists, there is a graceful way to build both WAR and JAR in one build.

In the maven WAR plugin configuration of the module that is to be built as WAR and JAR, add the following:

```xml
<attachClasses>true</attachClasses>
<classesClassifier>classes</classesClassifier>
```

This will build a JAR file with a qualifier. So wherever this JAR file is being used in the project, alone with `groupId`, `artifactId`, and `version`, specify the following as well:

```xml
<classifier>classes</classifier>
```

### More Resources

1. [https://www.baeldung.com/maven-dependencymanagement-vs-dependencies-tags](https://www.baeldung.com/maven-dependencymanagement-vs-dependencies-tags)
2. [https://guides.dataverse.org/en/latest/developers/dependencies.html](https://guides.dataverse.org/en/latest/developers/dependencies.html)
