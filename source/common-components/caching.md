# Caching

An application uses cache for 2 main reasons:

1. Retrieving data from cache typically tends to be faster than retrieving data from a data source like an RDBMS.
2. To offload the database from running too many queries frequently or from running some intensive queries frequently.

Our application used 2 different Redis stores to act as the caching system:

* default cluster – which contains data that is not persisted by the Redis system, and
* persist cluster – where the data is persisted by the Redis system so that the cached data is not volatile and will not be lost on restart of the Redis system.

All interactions with Redis is wrapped using the RedisService module:

1. RedisKeyTemplate - Define the Key, which cluster it belongs to, its expiry, etc.
2. RedisClusterService and its subclasses to interact with Redis.

Cache can also go out of sync with DB data. This is typically handled by helper methods: The helper methods for write will, on successful write to the DB, also delete the cached data. The helper methods for reading the data, will read from the cache, and update the cache with DB data if it is empty. \[This is also called write around cache strategy]

Multiple things can go wrong if care is not taken in using caching systems. Some of the common ones are these detailed below.

## Versioning keys

When changing the value of a Redis key, it is better to change the value of the key rather than burst the cache by deleting the key. The reasons being:

1. It avoids a manual step during deployments that could get missed and cause problems
2. In case of reverts, we do not have to worry about inconsistent data in cache keys
3. Avoid overloading the DB with queries

## Expiry of keys

Every cache must have an expiry. Some application data might need to be in cache for a small duration where they are queried frequently and then no used for a long duration afterwards. Key expiry ensures these data to not take up memory in the caching system unnecessarily. Having expiry also ensures that we can use versioning reliably.

## In-memory caching

Some data might be used so frequently that having a second level of cache can offer benefits in speed of execution. This is usually part of the application code by using some library.

Our application uses LoadingCache form Guava library for this purpose.

## Sharding of keys

In a redis cluster, keys are stored in a specific node by performing hashing on a key. When multiple keys are related, it is preferable to hash them to the same hash slot. Without this, performing multiple key operations will not be feasible. To guarantee that related keys are in the same slot, we can leverage the hash tags feature. For more info, refer [this](https://redis.io/docs/manual/scaling/#redis-cluster-data-sharding).

For using hash tags, we need to have a substring of the key in curly braces `{ }` . In our case, this is usually the app ID. They are defined in RedisKeyTemplate class in RedisService module.

## Atomicity of operations

In some cases, redis is used for locking or as a way to mark progress, etc. In these cases, the application might need to perform a read operation to check if the required checkpoint has reached or else write to redis to set the requisite data. When this operation is not atomic, we would end up with interleaving of operations that gives room for race conditions.

For instance, setting the key if absent is better than getting the value for a key, checking for null, and then setting it.
