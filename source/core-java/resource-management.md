# Resource management

## **Cookbook Scenarios**

### **Resource leaks**

#### **Problem Statement**

Resources like DB connections or open files can leak if not closed properly

#### **Solution**

Ensure that connections (to DB, queue, redis etc) or files or other resources are closed properly and in the right order. Leverage try-with-resources to ensure resources are closed automatically. Also nested try-with-resources blocks can be used to ensure that resources are closed in the right order – for example, ResultSet needs to be closed, followed by statement, and then DB connection. To be eligible to use try-with-resources, the resource must implament `Autocloseable` interface. In the rare case that a resource does not do that, prefer writing a wrapper class that does implement this interface and ensure only the wrapper is used for such connections.

:::{tip}
DO this
```java
public Data getDataFromDb(int dataId) {
    Data data = null;    //Connection by default implements Autocloseable    
    try (Connection dbConnection = connectionPool.getConnection();
        PreparedStatement statement = connection.getPreparedStatement(SQL)) {            
        statement.setInteger(1, dataId);
        //Nested try-with-resources ensures that the resources are closed properly            
        // and in the reverse of the order in which they were opened            
        try (ResultSet result = statement.execute()) {
            if (result.next()) { //or a while loop if when multiple results are expected
                data = new Data();
                data.setValue(result.getString(1);                
            }
        }
    } catch (SQLException e) {
        log.error("Error: ", e);
        throw RuntimeException(e);
    }    
    return data;
}
```
:::

### **Reusing rescources**

#### **Problem statement**

Too many resources are hogging the memory or resources are not opening because limit has been reached.

#### **Solution**

Ensure resources such as connections or threads are pooled. This ensures that there is only a configured number of resources that remain open and are reused optimally.
