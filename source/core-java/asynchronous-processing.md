# Asynchronous processing

## **Producers & Consumers**

## **Cookbook Scenarios**

### **Losing traceability in async processing**

#### **Problem Statement**

When the control goes through an async flow, it can be hard to trace the relevant logs and therefore debug the problem. This includes losing things like request ID or app ID.

#### **Solution**

Typically JMS will be used for sending messages from producer which in turn is used by consumer(s). In the produces, you can set these values in as message properties. When receiving the message in the consumer, parse the properties and set as logging MDC. (Make sure the MDC is cleared properly when the consumer code completes execution or if there is exceptions).

### **Concurrency issues in consumers**

#### **Problem Statement**

#### **Solution**

Consumers are usually concurrent. Ensure the consumer code is thread-safe. Refer [Concurrency & Parallelism](./concurrency-and-parallelism.md) page for more info.
