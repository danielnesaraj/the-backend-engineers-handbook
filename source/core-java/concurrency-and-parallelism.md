# Concurrency & Parallelism

There are multiple places where we need to pay attention to concurrency; particularly to ensure thread-safety – servlets, Runnable or Callable instances, methods that are used in multi-threaded context, queue/kafka consumers.

## **Thread-safety**

NOTE TO SELF: add content on thread safety

### **Configuration**

## **Cookbook Scenarios**

### **States in Runnables**

#### **Problem Statement**

Fields may be used to store states in a runnable. This is OK as long as the field is not mutable and cause sharing of variables.

1. In servlets, Runnable/Callable instances, do not have mutable states (both static and non-static ones) – if a field can be changed, then that can introduce sharing of variables.

:::{danger}
DON'T DO this
```java
 public class SomeLogic implements Runnable {
    private int value = 0; //mutable variable

    public void run() {  
        value = //some assignment
        .
        .
        someObject.someOtherLogic(value); // this will be an issue because the value is shared between threads!
    }
 }
```
:::

2. Final fields (both static and non-static) are fine as long as the object is immutable. Avoid mutable objects as it can introduce sharing of state.

:::{danger}
DON'T DO this
```java
public class SomeLogic implements Runnable {
    private final SomeModel someModel = new SomeModel(); //NOT immutable
    
    public void run() {
        .        
        .        
        someModel.setValue(value); // this will be an issue because the value is shared between threads!
    }
}
```
:::

#### **Solution**

**Solution 1:** Make sure that the fields are immutable

**Solution 2:** If you need to have fields that are mutable but also must be restricted to a thread, use ThreadLocal. Ensure that the variable is set and removed in the appropriate places.

:::{tip}
DO this
```java
public class SomeLogic implements Runnable {    
    private final ThreadLocal<Integer> localInt = new ThreadLocal<>();

    public void run() {
        localInt.set(0);
        .
        localInt.set(localInt.get() + 1);
        .
        localInt.remove();
    }
}
```
:::

### **Configuring thread pool**

#### **Problem Statement**

`ExecutorService` is used to schedule Runnable/Callable to be executed in multi-threaded way. But it is not optimally using the resources in the machine.

#### **Solution**

Keep the configuration of thread pool in properties because it needs to be configured based on the number of executable cores in the machine where the application needs to be run.

**Note:** The same goes for other concurrent mechanisms like Kafka consumers.

### **Configuring ExecutorService**

#### **Problem Statement**

When an `ExecutorService` is used to execute jobs in parallel, the jobs are first pushed into a queue. Many of the convenience factory methods utilise an open ended queue. This might cause the app to run out of memory.

#### **Solution**

This might be helpful in simplistic cases. But within the application code, always try to create a fixed size queue and retry scheduling when it fails because the queue is full.
