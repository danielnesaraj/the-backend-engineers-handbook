# The `static` modifier

## `Static` Field

When a field is declared static, the data is stored with the loaded class object. Only a few cases like logger references or constants need to be associated with the class itself.

For example, the following is the best way to declare a logger instance is:

```java
private static final Logger log = LoggerFactory.getLogger(Clazz.class); 

public static final String SOME_GLOBAL_CONSTANT = "constant value";
```

## `Static` Method

Static methods are usually used for utility purposes and are not meant for use as methods that contain business logic. Static methods make code very difficult to unit test.

## **Cookbook Scenarios**

### **Static states are shared**

#### **Problem statement**

Static fields are visible across threads. This can introduce concurrency issues.

#### **Solution**

In multithreaded cases, it is safe only if the static fields are also final and/or immutable.

:::{danger}
**AVOID static MUTABLE FIELDS.** Never do the following:

```
private static App app;
```
:::

### **Static dependencies are hard to mock**

#### **Problem Statement**

Static references for dependencies

#### **Solution**

Unless you are working with static methods, this should not be needed (next section explains how static methods can be avoided). If you are dealing with static methods, either refactor the method or instantiate the dependency inside the method.

:::{danger}
**NEVER CREATE `static`  OR `static final`  FIELD REFERENCES FOR SPRING BEANS.** Getting reference for a bean from SpringContext is cheap – using this in multiple places will not hurt performance.
:::

For more info on how to get bean instances, check out the Dependency Injection section.

### **Static methods are hard to test**

#### **Problem Statement**

The most common use-case of making a method static is for helper methods that do not operate depending on fields (states). However, static methods are:

* Impossible to mock with classic mocking frameworks like mockito
* Using powermock to mock static method will involve changing of byte code which in turn will cause problems in reporting coverage.

#### **Solution**

Prefer making singleton objects with instance methods or Spring Beans.
