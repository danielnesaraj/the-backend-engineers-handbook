# Boilerplate Code & Libraries

Avoid writing boilerplate code. For things like getters and setters, builders, etc use lombok annotations.
