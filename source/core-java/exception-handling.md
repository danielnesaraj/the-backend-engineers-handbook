# Exception Handling

### **Exception Handling**

At no point, swallow an exception. Make a choice to catch or throw the exception.

When the exception is caught, at the very least, it must be logged (with trace wherever it makes sense).

When bubbling up the exception, do not print stacktrace at each level.

Ensure that the scope of try block is minimal. Do not have all unrelated logic that would not throw exception inside the try block.

Catch the relevant exception and do not use the catch-all Exception to catch exception.
