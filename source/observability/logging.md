# Logging

## **Introduction**

## **Cookbook Scenarios**

### **Correct dependencies**

#### **Problem Statement**

Ensure that the right SLF4J and Logback are used in maven when creating a new module. These are added as libraries in the POM file.

#### **Solution**

Ensure uniformity by declaring dependencies it the parent POM. Do not override in child modules.

### Log file size 

#### **Problem Statement**

Log files are too large

#### **Solution**

Ensure that the right log rotation is configured in `logback.xml`

### Contextual information

#### **Problem Statement**

Not enough contextual info regarding the log statement

#### **Solution**

Contextual info like the account ID or thread name are best put in MDC. Leverage the MDC configuration in `logback.xml` and ensure that the MDC values are put and cleared in the correct places. Request and Response filters, finally block, etc are reliable ways to set and clear MDC values so that things like exceptions do no interfere and corrupt data that needs to be logged.

### Performance pitfalls 

#### **Problem statement**

Logging is also code that gets executed as part of the application code. Care must be taken to log only necessary information that will help us track and fix problems or to debug it.

#### **Solution 1**

Some log statements are only required occasionally and might only be required during debugging. Such logs should only be logged in appropriate log levels like debug, or trace. These levels must also be disabled in production by default.

As an extension, always check if the log level is enabled for debug or trace before logging. If there are performance impact in computing the parameters for the logging statement, then this computation will be avoided in production when the level is disabled.

#### **Solution 2**

Avoid using `String.format()` at all costs. This is inherently slower than the placeholder mechanism used by logging frameworks.

### Missing stacktrace 

#### **Problem statement**

Exceptions are usually logged in catch blocks and if not done properly, the stacktrace will not be printed. This will be a problem if the exception can be thrown from multiple places in the try block and make it very hard to arrive at root cause.

#### **Solution 1**

Ensure that in catch blocks the exception object (Throwable type) is the last argument and number of `{}` placeholders in log message is 1 less than the number of arguments.


```{code-block} java
:caption: Example
:lineno-start: 1

log.info("Doing something for user ID {}", userId);
log.error("Error doing something for user ID {}", userId, exception);
```

Leverage varargs overloaded method when there are more than 2 markups and exception to log

```{code-block} java
:caption: Example
:lineno-start: 1

log.info("Doing something for user ID {} app ID {} shard ID {}", userId, appId, shardId);
log.error("Error doing something for user ID {}", userId, appId, shardId, exception);
```

#### **Solution 2**

Narrow the scope of try block as much as possible. Having multiple try-catch blocks might also be preferable.

### More Resources

1. [https://www.baeldung.com/java-logging-intro](https://www.baeldung.com/java-logging-intro)
